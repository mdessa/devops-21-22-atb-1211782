# Class Assignment 3 - part 1 Report


## 1. Analysis, Design and Implementation

The resolution of the practical exercise CA3 - part2 is described on
the next points. 
The goal of this exercise is the use of the tool Vagrant in the implementation of virtual 
machines. 

Vagrant is a tool that allows us to create, configure, and manage boxes of virtual machines. 
Basically, it is a layer of software installed between a virtualization tool 
and a VM.

It shares environments and code as well which allows the code from one developer to work on the system of another.

The first contact with Vagrant was following training steps indicated 
on the file *devops06.pdf*. An initialization using the indicated box,
installing the apache, setting up the network ip where some tasks
executed.

On more advanced phase of the exercise I realized I could run said file due to lack of compatibility
with Java versions and the box being used. Therefore, I had to start all over, this time with the "bento/ubuntu-18.04".


## 1.1 **Implementation**

Initially it was used the vagrant file present on the 
repository *https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/*. This 
file creates and provisions two virtual machines:
* web: which is used to run tomcat and the spring boot basic 
application


 

* db: which is used to execute the H2 server database

 

### 1.1.1 *react-and-spring-data-rest-basic*

To achieve the goal of running the spring application, it was necessary 
to replace the repository in use in the Vagrant file.

These commands will navigate in the virtual machine as if we were on the actual machine's terminal.
We need clone the intended repository in navigate into the folder on which we wish to boot the application.

>git clone https://mdessa@bitbucket.org/mdessa/devops-21-22-atb-1211782.git
> 
>cd devops-21-22-atb-1211782
> 
>cd CA2
> 
>cd part2
> 
>cd react-and-spring-data-rest-basic>
> 
>chmod u+x gradlew
> 
>./gradlew clean build




It was also needed to make some adjustments to the application in order for it to run properly.
These modifications were made based on the repository provided.

Once these steps were complete the following command was used to run the file:
>vagrant up

After the process was finished, it was possible to check if the virtual machines were running 
by executing the following commad:
>vagrant status

Or by accessing the Virtual Box Manager which shows the boxes we have in use and allows us to manage them.

To confirm the application was running correctly, a browser was used to verify both machines output.

The web application:
>http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/

and the database:
>http://localhost:8082/

On this last one, it was also possible to login and run SQL commands.

## 2. Analysis of an Alternative

Hypervisor is a software that allows you to run one or multiple virtual
machines with their own operating systems (guest operating systems) on
a physical computer, which is called a host machine.

There are two types of hypervisors:

* Type 1: hypervisors run directly on the system hardware –
  A “bare metal” embedded hypervisor, for example HyperV.
* Type 2: hypervisors run on a host operating system that provides
  virtualization services, such as I/O device support and memory management.

### 2.1 VirtualBox vs VMware



VirtualBox and VMware workstation are a type 2 hypervisor that are 
also called as  hosted hypervisor. A type 2 hypervisor is an application 
that runs on the operating system (OS). When a physical computer starts,
the operating system installed on the host loads normally and the 
hypervisor is inactive. A user starts the hypervisor application and 
then starts the needed virtual machines and VM hosted processes are 
created.

Some differences between VirtualBox and VWware are indicated on the 
table below:

|     | VirtualBox | VMware |
|-----|------------|--------|
|  Durability of the environment|     slow in production or testing environment.        |   Swift in utilizing the resources of the host machine.      |
| Interface    | user friendly interface.           |    Slightly complicated user interface than VirtualBox.   |
|    Target Audience |       Suitable for developers, testers, students and home use.    | Can be complicated if end user is not a system engineer.       |
|  Hardware and software virtualisation   | Provides virtualization for both hardware and software         |   Provides virtualization only for hardware.      |




