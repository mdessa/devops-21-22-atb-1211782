# Class Assignment 1 Report

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered
in Bitbucket. See how to write README files for Bitbucket
in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the
folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

*A section dedicated to the description of the analysis, design and
implementation of the requirements*

*Should follow a "tutorial" style (i.e., it should be possible to
reproduce the assignment by following the instructions in the
tutorial)*

* *Should include a description of the steps used to achieve the
  requirements*
* *Should include justifications for the options (when required)*

## 1.1 **CA3-part1**



"1. You should start by creating your VM as described in the lecture"

This step was done following the lecture's instructions as indicated.

"2. You should clone your individual repository inside the VM"
Copied the repository link in bitbucket and, through  my host machine's command line connected to my vm's
console via ssh, cloned it to my vm.

"3. You should try to build and execute the spring boot tutorial basic project and the
gradle_basic_demo project"

For each assigment I accessed the correspondent project folder in the console and ran the builder

CA1 example:

> *mario@ubuntu:~/devops-21-22-atb-1211782/CA1/tut-react-and-spring-data-rest/basic$ ./mvnw spring-boot:run*

With the application running in the vm I then accessed it  on my host machine's browser in order
to confirm it. This had to be done because the vm has no graphic interface which is required to exhibit the application result.
This time, instead of  the url localhost/8080, I needed to use my vm's ip address followed by /8080 
since the server was no longer running on my host machine but in the vm.

CA2-part1 example


> *mario@ubuntu:~/devops-21-22-atb-1211782/CA2/part1/gradle_basic_demo$ ./gradlew build*

Once the application was running in vm, I then ran the runServer task.

> *./gradlew runServer*

I then went on the host machine and run the runClient task. In order for it to run properly I had to replace "localhost" 
in the args for the vm ip address since this one was running the server task now.

> *./gradlew runClient*

Once it ran, the chat window appeared and as soon as I logged in, the vm console show the following message:

> *15:37:40.508 [pool-1-thread-1] INFO  basic_demo.ChatServer.Handler - A new user has joined: dessa*


Next I tried inverting the tasks in vm and host machine. i.e I executed the RunServer on my host machine and 
the runClient on the vm.
The following exception occurred:

>Exception in thread "main" java.awt.HeadlessException:
No X11 DISPLAY variable was set, but this program performed an operation which requires it.*

This exception occurred due to the fact that the runClient requires a graphic interface, which the vm does not have.
Therefore, it is possible to run the application server with a vm such as the one used because the server
doesn't require the interface.
For the client, it has to be one with said interface, making no difference whether it's a virtual or physical machine. 

CA2-part2 example

> *mario@ubuntu:~/devops-21-22-atb-1211782/CA2/part2/gradle_basic_demo$ ./gradlew build*

> *mario@ubuntu:~/devops-21-22-atb-1211782/CA2/part2/gradle_basic_demo$ ./gradlew copyDocs*

After executing the task copyDocs I listed the contents of the folder "dist"

> *mario@ubuntu:~/devops-21-22-atb-1211782/CA2/part2/react-and-spring-data-rest-basic/dist$ ls -al*
 
I could then verify that the ".jar" files were in said folder. 



After executing the task deleteWebPack I listed the contents of the folder "static"

> *mario@ubuntu:~/devops-21-22-atb-1211782/CA2/part2/gradle_basic_demo$ cd src/resources/static*
> *mario@ubuntu:~/devops-21-22-atb-1211782/CA2/part2/gradle_basic_demo$ ls -al


I could then verify that the built folder had been deleted.











