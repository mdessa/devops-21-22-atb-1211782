package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;

class EmployeeTest {


    @org.junit.jupiter.api.Test
    void getFirstName() {

        //arrange

        String firstName = "Asdrubal";
        String lastName = "Barca";
        String description = "Nova Cartago Boss";
        int jobYears = 4;
        String email = "email@email.com";

        Employee employee = new Employee(firstName, lastName, description, jobYears, email);

        String expected = "Asdrubal";

        //act

        String actual = employee.getFirstName();

        //assert

        Assertions.assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void setFirstName() {
        //arrange

        String firstName = "Asdrubal";
        String lastName = "Barca";
        String description = "Nova Cartago Boss";
        int jobYears = 4;
        String email = "email@email.com";

        Employee employee = new Employee(firstName, lastName, description, jobYears,email );

        String expected = "Super Asdrubal";

        employee.setFirstName("Super Asdrubal");

        //act

        String actual = employee.getFirstName();

        //assert

        Assertions.assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void getLastName() {

        //arrange

        String firstName = "Asdrubal";
        String lastName = "Barca";
        String description = "Nova Cartago Boss";
        int jobYears = 4;
        String email = "email@email.com";

        Employee employee = new Employee(firstName, lastName, description, jobYears, email);

        String expected = "Barca";

        //act

        String actual = employee.getLastName();

        //assert

        Assertions.assertEquals(expected, actual);
    }


    @org.junit.jupiter.api.Test
    void setLastName() {
        //arrange

        String firstName = "Asdrubal";
        String lastName = "Barca";
        String description = "Nova Cartago Boss";
        int jobYears = 4;
        String email = "email@email.com";

        Employee employee = new Employee(firstName, lastName, description, jobYears,email );

        String expected = "Barca Velha";

        employee.setLastName("Barca Velha");

        //act

        String actual = employee.getLastName();

        //assert

        Assertions.assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void getDescription() {

        //arrange

        String firstName = "Asdrubal";
        String lastName = "Barca";
        String description = "Nova Cartago Boss";
        int jobYears = 4;
        String email = "email@email.com";

        Employee employee = new Employee(firstName, lastName, description, jobYears, email);

        String expected = "Nova Cartago Boss";

        //act

        String actual = employee.getDescription();

        //assert

        Assertions.assertEquals(expected, actual);


    }

    @org.junit.jupiter.api.Test
    void setDescription() {
        //arrange

        String firstName = "Asdrubal";
        String lastName = "Barca";
        String description = "Nova Cartago Boss";
        int jobYears = 4;
        String email = "email@email.com";

        Employee employee = new Employee(firstName, lastName, description, jobYears, email);

        String expected = "Nova Cartago Super Boss";

        employee.setDescription("Nova Cartago Super Boss");

        //act

        String actual = employee.getDescription();

        //assert

        Assertions.assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void getJobYears() {
        //arrange

        String firstName = "Asdrubal";
        String lastName = "Barca";
        String description = "Nova Cartago Boss";
        int jobYears = 4;
        String email = "email@email.com";

        Employee employee = new Employee(firstName, lastName, description, jobYears, email);

        int expected = 4;

        //act

        int actual = employee.getJobYears();

        //assert

        Assertions.assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void setJobYears() {
        //arrange

        String firstName = "Asdrubal";
        String lastName = "Barca";
        String description = "Nova Cartago Boss";
        int jobYears = 4;
        String email = "email@email.com";

        Employee employee = new Employee(firstName, lastName, description, jobYears, email);

        int expected = 5;

        employee.setJobYears(5);

        //act

        int actual = employee.getJobYears();

        //assert

        Assertions.assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void getEmail() {
        //arrange

        String firstName = "Asdrubal";
        String lastName = "Barca";
        String description = "Nova Cartago Boss";
        int jobYears = 4;
        String email = "email@email.com";

        Employee employee = new Employee(firstName, lastName, description, jobYears, email);

       String expected = "email@email.com";

        //act

        String actual = employee.getEmail();

        //assert

        Assertions.assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void setEmail() {
        //arrange

        String firstName = "Asdrubal";
        String lastName = "Barca";
        String description = "Nova Cartago Boss";
        int jobYears = 4;
        String email = "email@email.com";

        Employee employee = new Employee(firstName, lastName, description, jobYears, email);

        String expected = "emailtest@email.com";

        employee.setEmail("emailtest@email.com");

        //act

        String actual = employee.getEmail();

        //assert

        Assertions.assertEquals(expected, actual);
    }


}