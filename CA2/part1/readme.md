# Class Assignment 1 Report

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered
in Bitbucket. See how to write README files for Bitbucket
in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the
folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

*A section dedicated to the description of the analysis, design and
implementation of the requirements*

*Should follow a "tutorial" style (i.e., it should be possible to
reproduce the assignment by following the instructions in the
tutorial)*

* *Should include a description of the steps used to achieve the
  requirements*
* *Should include justifications for the options (when required)*

## 1.1 **CA2-part1**

Started by copying the repository provided to a folder named "part1" in a new folder named "CA2" as indicated.
Once this project was opened in the IDEA, the readme.md file was created and these changes added and then  commited to git.  

"3. ADD A NEW TASK TO EXECUTE THE SERVER"

The following task was added to build.gradle

> *task runClient(type:JavaExec, dependsOn: classes){
>group = "DevOps"
>description = "Launches a chat client that connects to a server on localhost:59001 "
>
>   classpath = sourceSets.main.runtimeClasspath
>
>    mainClass = 'basic_demo.ChatClientApp'
>
>    args 'localhost', '59001'
}*

These changes were then committed and pushed to git using the following commands:

> *git commit -a -m "runClient task added to build.gradle. Adresses #3"*

and

> *git push origin master*

"4. Add a simple unit test and update the gradle script so that it is able to execute the
test."

Test created and junit 4.12 dependency added in gradle.

These changes were then committed and pushed to git using the following commands:

> *git commit -a -m "unit test added to AppTest. addresses #4*

and

> *git push origin master*


"5. Add a new task of type Copy to be used to make a backup of the sources of the
application."

> *dependencies {
// Use Apache Log4J for logging
implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
implementation 'junit:junit:4.13.1'
}*

These changes were then committed and pushed to git using the following commands:

> *git commit -a -m "copyTask task created. addresses #5"*

and

> *git push origin master*


"6. Add a new task of type Zip to be used to make an archive (i.e., zip file) of the
sources of the application."

zip task created in build.gradle

> *task zip(type:Zip){
from 'src'
into 'backup.zip'
destinationDir(file('backupZip'))
}*

After running the task, a zip file containing a copy of src was created, 
 which was stored in a new folder named backupZip.

These changes were then committed and pushed to git using the following commands:

> *git commit -a -m "zip task created. addresses #6"*

and

> *git push origin master*

"7. At the end of the part 1 of this assignment mark your repository with the tag
ca2-part1."

> *git tag ca2-part1*








