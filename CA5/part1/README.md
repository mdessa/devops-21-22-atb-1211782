# Class Assignment 5 - part 1 Report


## 1. Analysis, Design and Implementation

The resolution of the practical exercise CA5 - part1 is described on
the next points. 
The goal of this exercise is to practice with Jenkins by creating a simple pipeline using the "gradle
basic demo" from CA2-part1.






## 1.1 **Implementation**

Giving the two alternatives, using a Docker container and running directly the War file, the latter was chosen.
Following the instructions found on the WAR file section of the link provided: 




>https://jenkins.io/doc/book/installing/

A WAR file was downloaded in order to install Jenkins.

The following command was executed to install

>java -jar jenkins.war

Accessing localhost:8080 allowed to enter Jenkins dashboard to continue the setup.

Then, a Jenkins file was created to create a pipeline that will build the gradle
basic demo  app.

This was done by follwing the instructions in "Continuous Integration" lecture.

This file had to be adjusted for the repository to be used as for directory for the app to assemble within said repository.



>stage('Checkout') {
> steps {
>    echo 'Checking out...'
>   git url: 'https://bitbucket.org/mdessa/devops-21-22-atb-1211782'
> }

*

>stage('Assemble') 
>{
>steps {
>echo 'Assembling...'
>dir ("CA2/part1/gradle_basic_demo") {
>script {
>if (isUnix()){
>sh 'chmod +x gradlew'
>sh './gradlew assemble'}
>else
>bat './gradlew assemble'
>}

Next step it would be to run the tests in the app

>stage('Test') {
>steps {
>echo 'testing...'
>dir ("CA2/part1/gradle_basic_demo"){
>script {
>if (isUnix()){
>sh 'chmod +x gradlew'
>sh './gradlew test'
>junit 'build/test-results/test/*.xml'}
>else
>{
>bat './gradlew test'
>junit 'build/test-results/test/*.xml'}
>}
>}
>}
>}

Last step archives in Jenkins the archive files (generated during Assemble)

>stage('Archiving') {
>steps {
>echo 'Archiving...'
>dir ("CA2/part1/gradle_basic_demo"){
>archiveArtifacts 'build/distributions/*'
>}
>}
>}

Each of the previous diferentiated stages define each step required in the exercise. This is done so each step is executed one at each time instead of one single step (i.e assemble).

For each one it is defined the directory of the app within the repository and then the Graddle commands to be executed. There are two sets of commands. One for Unix systems and another for Windows systems. This allows for the file to be run in any machine no matter the operating system in use and, consequentely, the correct commands to execute.

Finally, in Jenkins dashboard (localhost:8080), under "build now", the build was made and it can be seen for each step if it was successfull and how long it took.

![devops Readme](devopsreadme.png)