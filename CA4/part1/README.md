# Class Assignment 4 - part 1 Report


## 1. Analysis, Design and Implementation

The resolution of the practical exercise CA4 - part1 is described on
the next points. 
The goal of this exercise is to create docker
images and to run containers using the chat application from CA2 

"Docker is a technology that allows you to incorporate and store your code and its dependencies into package – an image.
This image can then be used to spawn an instance of your application – a container. 
The fundamental difference between containers and Virtual Machines is that containers don’t contain a hardware hypervisor."

(source: https://www.aquasec.com/cloud-native-academy/docker-container/)




## 1.1 **Implementation**

Creating the docker file was quite similar to creating a vagrant file.
It had to be given the information of what OS (image) to run, specify which repository to use, the project to run and which actions to take.
after defining all commands to run, define the exposed port to use.


OS
>FROM ubuntu

application update and installing git

>RUN apt-get update -y && apt-get install -y git \

installing java

>openjdk-11-jdk-headless \

clean repositories 

>&& apt-get clean\

clean temporary files
>&& rm -rf /var/lib/apt/lists/*

cloning git repository

>RUN git clone https://mdessa@bitbucket.org/mdessa/devops-21-22-atb-1211782.git

directory of the app to run

>WORKDIR /tmp/devops-21-22-atb-1211782/CA2/part1/gradle_basic_demo

change gradle wrapper permissions

>RUN chmod u+x gradlew

build the app

>RUN ./gradlew clean build 

port to expose
>EXPOSE 59001

command to be executed after the container is initialized. 
it executes the server  app

> CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001  

then, the container image was push to the docker hub using the following command

>docker push maclade/docker_excercise_image:latest









