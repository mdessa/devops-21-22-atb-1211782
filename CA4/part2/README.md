

Class Assignment 4 - part 2 Report
1. Analysis, Design and Implementation
The resolution of the practical exercise CA4 - part2 is described on the next points. The goal of this exercise is to produce a solution similar to the one of the part 2 of the previous CA but now using Docker instead of Vagrant.

In order to achieve the goal it was used docker compose to produce two containers, one for web and one for database.

1.1 Implementation
To implement the goal, the files made available in the class were used. However,some changes add to be made both in the files and the application.

The files had to be adjusted for the repository to be use as for the directory to the application to be run.

*repository to be used

RUN git clone https://mdessa@bitbucket.org/mdessa/devops-21-22-atb-1211782.git
*application directory

WORKDIR /tmp/build/devops-21-22-atb-1211782/CA2/part2/react-and-spring-data-rest-basic
The application had to be updated to the ip used in the compose file.

on the application properties the givern ip was used

jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
Once this was done the following command was used

docker-compose up
This command will run the containers. the web container will be used to run Tomcat and the spring application and the data base container will o execute the H2 server database.

Finally, accessing

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/

will demonstrate the application is running accessing the table created in CA2-part2.












